SanberApp

App ini melanjutkan dari mockup saya pada https://www.figma.com/file/fLaq6GUYJHYDJkbmnZS3kO/Mockup-1?node-id=0%3A1,
tetapi ada sedikit penyesuaian pada desain untuk menyesuaikan dengan API yang saya gunakan

App terdiri dari 5 halaman:
1. Flash Screen
2. Login Screen, bisa diisi username dan password apa saja
3. Home Screen yang berisi daftar skill Dev
4. About Screen yang berisi foto, dan social media serta link untuk masuk halaman Portfolio
5. Portfolio Screen yang berisi daftar portfolio Dev

App menggunakan:
1. Redux untuk State Management
2. API dari https://gitconnected.com/v1/portfolio/deczen untuk mengambil data skill, nama, foto, dan portfolio
3. Menggunakan Tab navigasi pada bagian bawah layar

- APK bisa didownload pada https://drive.google.com/drive/folders/1vajthQ8CdR1GF7wgls14P8ete-7BRoTB?usp=sharing
- Video bisa ditonton pada https://youtu.be/SYfD7sJb84g