
export const actionCreators = {
    set: (data) => {
        return { type: 'set', payload: data }
    },
}
  
export const reducer = (state = {data:[]}, action) => {
    const { data } = state
    const { type, payload } = action

    switch (type) {
        case 'set': {
            return {
                data: payload,
            }
        }
    }

    return state
}