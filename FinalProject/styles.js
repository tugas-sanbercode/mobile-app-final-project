import React from 'react'
import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerView: {
        paddingTop:20,
        paddingLeft: 30,
        paddingRight: 30
    },
    topHeader: {
        paddingTop: 30,
        paddingBottom:10,
        paddingLeft:20,
        paddingRight:20,
        elevation: 3,
         textAlign:'right',
        backgroundColor: 'white',
    },
    appTitle: {
        textAlign: 'right',
        fontSize: 16,
    },
    screenTitle: {
        textAlign: 'center',
        fontSize: 30,
        marginVertical: 20
    },
    logo: {
        marginVertical: 20,
        width: 320,
        height: 90,
    },
    subTitle:{
        fontSize:15,
        marginBottom: 10,
        fontWeight: '600'
    },
    formInput: {
        marginVertical:10,
        marginHorizontal:10
    },
    input: {
        height:40,
        borderColor:'#000',
        borderWidth: 1,
        backgroundColor: '#E9E9E9',
        paddingLeft:10,
        paddingRight:10,
        borderRadius:6
    },
    mainBtn: {
        backgroundColor: '#24B0FF',
        padding:10,
        alignItems: 'center',
        width:120,
        borderRadius:6,
    },
    mainBtnText: {
        fontSize: 16,
        color: '#fff'
    },
    linkText: {
        color: '#0292E3',
        textDecorationLine: 'underline',
        fontSize:16
    },
    profileImage: {
        width:300,
        height:300
    },
    profileName: {
        fontSize:20,
        fontWeight:'bold'
    },
    socialIcon: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginVertical:4
    },
    socialIconText: {
        fontSize:18,
        marginLeft:10
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center',  
        width: '33%',
        paddingTop:5,
    },
    tabTitle: {
          fontSize: 10,
          color: 'rgb(142, 142, 143)',
          paddingTop:4,
    },
    errorText: {
        color: 'red',
        fontWeight:'600',
        marginBottom:10,
    },
    hiddenErrorText: {
        display:'none'
    },
    skillTitle: {
        fontWeight: 'bold',
    },
    skillLevel: {
        fontStyle: 'italic',
        color: 'rgb(146,146,146)'
    },
    skillExperience: {
        fontSize: 12,
        color: 'rgb(146,146,146)'
    },
    projectImage: {
        width:200,
        height:60
    },
    projectName: {
        fontWeight: 'bold'
    },
    projectSummary: {
        color: 'rgb(146,146,146)'
    },
    projectWebsite: {
        fontSize: 12,
        fontStyle: 'italic',
        color: 'rgb(146,146,146)'
    }
})

export default styles;