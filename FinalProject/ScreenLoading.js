import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux'
import {
    View, 
    Text, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform,
    ActivityIndicator
} from 'react-native'
import { actionCreators } from './ProfileRedux'
import mapStateToProps from './mapStateToProps'
import styles from './styles.js'
import TabBar from './TabBar'

const ScreenLoading = (state) => {
    const {navigation} = state
    const setData = (data) => {
        const { dispatch } = state    
        return dispatch(actionCreators.set(data))
    }

    useEffect(() => {
        fetch('https://gitconnected.com/v1/portfolio/deczen')
        .then((response) => response.json())
        .then((json) => {
            console.log(json)
            setData(json)
            navigation.push('HomeStackScreen', { name: 'Home'}) 
        })
        .catch((error) => console.error(error))
    }, []);

    return (
        <View style={{ flex: 1, flexDirection: 'row',  justifyContent: 'center',   }}>
            <ActivityIndicator size="large" color="#ccc" />
        </View>
    )
}

export default connect(mapStateToProps)(ScreenLoading);