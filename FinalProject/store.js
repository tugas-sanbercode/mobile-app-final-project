import { createStore } from 'redux'
import { reducer } from './ProfileRedux'

const store = createStore(reducer);

export default store;