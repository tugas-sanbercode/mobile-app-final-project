import React from 'react'
import {
    View, 
    Text, 
    Image, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform
} from 'react-native'
import {connect} from 'react-redux'
import mapStateToProps from './mapStateToProps'
import styles from './styles.js'
import { AntDesign, Entypo } from '@expo/vector-icons'; 
import TabBar from './TabBar'

const AboutScreen = ({navigation, data}) => {

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.profileImage} resizeMode="contain" source={{uri:data.basics.picture}} />
                        </View>
                        <View>
                            <Text style={styles.profileName}>{data.basics.name}</Text>
                            <Text style={styles.profileTitle}>{data.basics.label}</Text>
                        </View>
                        <View style={{marginTop:20}}>
                            <View style={styles.socialIcon}>
                                <Entypo name="facebook-with-circle" size={30} color="#3b5998" />
                                <Text style={styles.socialIconText}>/deczen</Text>
                            </View>
                            <View style={styles.socialIcon}>
                                <Entypo name="twitter-with-circle" size={30} color="#1DA1F2" />
                                <Text style={styles.socialIconText}>@deczen</Text>
                            </View>
                            <View style={styles.socialIcon}>
                                <Entypo name="instagram-with-circle" size={30} color="#bc2a8d" />
                                <Text style={styles.socialIconText}>@deczen</Text>
                            </View>
                        </View>
                        <View style={{alignItems:'flex-start', marginTop:20}}>
                            <TouchableOpacity>
                                <Text style={styles.linkText} onPress={() => navigation.push('PortfolioStackScreen', { name: "Portfolio" })}>Portfolio</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
            {/* <TabBar navigation={navigation}/> */}
        </KeyboardAvoidingView>
    )
}

export default connect(mapStateToProps)(AboutScreen);