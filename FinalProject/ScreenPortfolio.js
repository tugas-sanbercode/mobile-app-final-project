import React from 'react'
import {
    View, 
    Text, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform
} from 'react-native'
import { connect } from 'react-redux'
import mapStateToProps from './mapStateToProps.js'
import styles from './styles.js'
import TabBar from './TabBar'

const ScreenPortfolio = ({navigation,data}) => {

    const {projects} = data

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            {/* <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
             </View> */}
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        {projects.map( (el, i) => {
                           
                            return (
                            <View style={{marginBottom:40,alignItems:'center'}} key={i}>
                                <View style={{alignItems:'center'}}>
                                    <Image style={styles.projectImage} resizeMode="contain" source={{uri:el.images[0].resolutions.mobile.url}} />
                                </View>
                                <Text style={styles.projectName}>{el.displayName}</Text>                             
                                <Text style={styles.projectSummary}>{el.summary}</Text>
                                <Text style={styles.projectWebsite}>{el.website}</Text>  
                            </View>
                            )
                        })}
                    </View>
                </View>
            </ScrollView>
            {/* <TabBar navigation={navigation}/> */}
        </KeyboardAvoidingView>
    )
}

export default connect(mapStateToProps)(ScreenPortfolio)