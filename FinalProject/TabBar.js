import React from 'react'
import {
    View, 
    Text, 
    TouchableOpacity, 
} from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 
import styles from './styles.js'

const TabBar = ({navigation}) =>(
    <View style={styles.tabBar}>
        <TouchableOpacity 
            style={styles.tabItem} 
            onPress={() => 
                navigation.push('ScreenHome', { name: "Home" })
            }>
                <AntDesign name="home" size={24} color="black" />
                <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => 
                navigation.push('ScreenAbout', { name: "About" })
            }>
                <AntDesign name="profile" size={24} color="black" />
                <Text style={styles.tabTitle}>About</Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => 
                navigation.push('ScreenLogin', { name: "Home" })
            }>
                <AntDesign name="logout" size={24} color="black" />
                <Text style={styles.tabTitle}>Logout</Text>
        </TouchableOpacity>
    </View>
)

export default TabBar