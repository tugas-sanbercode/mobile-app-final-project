import React, { useState } from 'react'
import {
    View, 
    Text, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform,
    Alert
} from 'react-native'
import styles from './styles.js'

const ScreenLogin = ({navigation}) => {

    const [input, setInput] = useState({
        username: '',
        password: '',
    });
    const [isError, setError] = useState(false);

    const loginHandler = () =>{
        
        setInput(input)

        let username = input.username;
        let password = input.password;

        console.log(input);

        if( username=='' || password==''){

            // Alert.alert(
            //     'Error',
            //     'Please fill username and password',
            //     [
            //         { text: "OK", onPress: () => console.log("OK Pressed") }
            //     ],
            //     { cancelable: true }
            // );

            setError(true);  
        }else{
            setError(false);            
        
          navigation.push('ScreenLoading', { name: 'Loading'})
        }
        
    }

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View style={{alignItems:'center'}}>
                            <Image style={styles.logo} source={require('./images/logo.png')} />
                        </View>
                        <View>
                            <Text style={styles.screenTitle}>LOGIN</Text>
                        </View>
                        <View style={styles.formInput}>
                            <Text>Username/email</Text>
                            <TextInput 
                                style={styles.input}
                                onChangeText={(username=>{ 
                                    input.username=username
                                    setInput(input) 
                                })} />
                        </View>
                        <View style={styles.formInput}>
                            <Text>Password</Text>
                            <TextInput 
                                style={styles.input} 
                                secureTextEntry={true}
                                onChangeText={(password=>{ 
                                    input.password=password
                                    setInput(input) 
                                })} />
                        </View>
                        <View style={{alignItems:'center', marginTop: 20}}>
                            <Text style={isError ? styles.errorText : styles.hiddenErrorText}>Please fill username & password</Text>
                            <TouchableOpacity style={styles.mainBtn} onPress={() => loginHandler()}>
                                <Text style={styles.mainBtnText} >
                                Login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default ScreenLogin;