import React from "react";
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ScreenSplash from './ScreenSplash';
import ScreenLoading from './ScreenLoading';
import ScreenLogin from './ScreenLogin';
import ScreenHome from './ScreenHome';
import ScreenAbout from './ScreenAbout';
import ScreenPortfolio from './ScreenPortfolio';
import mapStateToProps from './mapStateToProps'
// import Logout from './Logout'
import {
  Text, 
  TouchableOpacity, 
} from 'react-native'
import { AntDesign } from '@expo/vector-icons'; 
import styles from './styles.js'

const LoginStack = createStackNavigator();
const HomeStack = createStackNavigator();
const PortfolioStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const LoginStackScreen = (state) => (
  <LoginStack.Navigator screenOptions={{
    headerShown: false
  }}>
    <LoginStack.Screen name="ScreenSplash" component={ScreenSplash} />   
    <LoginStack.Screen name="ScreenLogin" component={ScreenLogin} />   
    <LoginStack.Screen name="ScreenLoading" component={ScreenLoading} />  
    <LoginStack.Screen name="HomeStackScreen" component={HomeStackScreen} />
    
  </LoginStack.Navigator>
)

const HomeStackScreen = ({navigation}) =>(
  <HomeStack.Navigator screenOptions={{
    headerShown: false
  }}>     
    <HomeStack.Screen name="TabsScreen" component={TabsScreen} />    
    <HomeStack.Screen name="PortfolioStackScreen" component={PortfolioStackScreen} />
  </HomeStack.Navigator>
)

const PortfolioStackScreen = ({navigation}) => (
  <PortfolioStack.Navigator screenOptions={{
    headerShown: true
  }}>
     <PortfolioStack.Screen name="ScreenPortfolio" component={ScreenPortfolio} />
  </PortfolioStack.Navigator>
)

const Logout = () => {
  return null
}

const TabsScreen = ({navigation}) => (  
  <Tabs.Navigator screenOptions={{
      headerShown: false
    }}>
      <Tabs.Screen name="ScreenHome" 
        component={ScreenHome} 
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <AntDesign name="home" size={24} color={color} />
          ),
        }}
      />
      <Tabs.Screen 
        name="ScreenAbout" 
        component={ScreenAbout}
        options={{
          tabBarLabel: 'About',
          tabBarIcon: ({ color }) => (
            <AntDesign name="profile" size={24} color={color} />
          ),
        }}
      />
      <Tabs.Screen 
        name="Logout" 
        component={Logout} 
        options={{
          tabBarLabel: 'Logout',
          tabBarIcon: ({ color }) => (
            <AntDesign name="logout" size={24} color={color} />
          ),
          tabBarButton: () => (
            <TouchableOpacity 
              style={styles.tabItem}
              onPress={() => 
                  navigation.push('ScreenLogin', { name: "Home" })
              }>
                  <AntDesign name="logout" size={24} color="rgb(142, 142, 143)" />
                  <Text style={styles.tabTitle}>Logout</Text>
            </TouchableOpacity>
          )
        }}
      />
  </Tabs.Navigator>
)

const App = (state) => (
  <NavigationContainer>
    <LoginStackScreen state={state} />
  </NavigationContainer>
)

export default connect(mapStateToProps)(App)