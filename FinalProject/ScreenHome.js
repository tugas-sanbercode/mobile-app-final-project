import React, {useState, useEffect} from 'react'
import {
    View, 
    Text, 
    Image, 
    TextInput, 
    TouchableOpacity, 
    KeyboardAvoidingView, 
    ScrollView,
    Platform,
    ActivityIndicator
} from 'react-native'
import {connect} from 'react-redux'
import mapStateToProps from './mapStateToProps'
import { MaterialIcons } from '@expo/vector-icons';
import styles from './styles.js'
import TabBar from './TabBar'

const ScreenHome = ({navigation, data}) => {
   
    const {skills} = data

    return (
        <KeyboardAvoidingView 
            behavior={Platform.OS == 'ios' ? "padding" : "height"}
            style={styles.container}    
        >
            <View style={styles.topHeader}>
                <Text style={styles.appTitle}>SanberApp</Text>
            </View>
            <ScrollView>
                <View style={styles.containerView}>
                    <View style={styles.mainBody}>
                        <View>
                            <Text style={styles.profileName}>{data.basics.name}</Text>
                        </View>
                        <View style={{marginTop:20}}>
                            <Text style={styles.subTitle}>Skills:</Text>
                        </View>
                        <View>
                            {skills.map( (el, i) => {
                            
                                return (
                                <View key={i} style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                                    <MaterialIcons name="stars" size={15} color="black" style={{paddingTop:3,paddingRight:10}} />
                                    <View style={styles.skillList}>
                                        <Text style={styles.skillTitle}>{el.name}</Text>                             
                                        <Text style={styles.skillLevel}>{el.level}</Text>
                                        <Text style={styles.skillExperience}>experience: {el.yearsOfExperience} year(s)</Text>  
                                    </View>
                                </View>
                                )
                            })}
                        </View>
                    </View>
                </View>
            </ScrollView>
            {/* <TabBar navigation={navigation}/> */}
        </KeyboardAvoidingView>
    )
}

export default connect(mapStateToProps)(ScreenHome);