import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const ScreenSplash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.navigate("ScreenLogin")
        }, 1000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
           <Image
        style={styles.tinyLogo}
        source={{
          uri: 'https://reactnative.dev/img/tiny_logo.png',
        }}
      />
            <Text style={{fontSize: 30}}>Splash Screen</Text>
        </View>
    )
}

export default ScreenSplash

const styles = StyleSheet.create({
    tinyLogo: {
        width: 50,
        height: 50,
      },
})
