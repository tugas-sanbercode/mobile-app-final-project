import React from 'react'
import { Provider } from 'react-redux'
import store from './store'

// Import komponen App
import App from './App'

// Memasukkan store pada Provider
export default function AppWithStore() {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  )
}